import {requestUserSuccess} from '../Actions/actions';
import { put, call, takeEvery } from "redux-saga/effects";
  
  export function* watchFetchUser() {yield takeEvery("FETCHED_USER", fetchUserAsync)}
  
  function* fetchUserAsync() {
      yield put(requestUserSuccess(yield call(() => { return fetch(`https://jsonplaceholder.typicode.com/users/1`, ).then((res) => res.json())})));
  }
  