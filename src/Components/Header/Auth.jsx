import { authenticate } from "../../Actions/actions";
import { connect } from "react-redux";

const Auth = (props) =>{
    const handleChange = () => {
        props.authenticate()
    }

    return (
        <div>
           <button onClick={handleChange}>{props.auth.isAuth ? "Logout" : "Login"}</button>
        </div>
    );
}

const mapStateToProps = (state) => ({
    auth: state.auth
})

export default connect(mapStateToProps, {authenticate})(Auth);
