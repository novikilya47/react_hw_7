import {createStore, combineReducers, applyMiddleware} from "redux";
import createSagaMiddleware from 'redux-saga';
import {watchFetchUser} from '../Saga/Saga';

import profileReducer from '../Reducers/Profile.reducer';
import productReducer from '../Reducers/Product.reducer';
import authReducer from '../Reducers/Auth.reducer';

const middlware = createSagaMiddleware();

export const rootReducer = combineReducers({
    profile: profileReducer,
    product: productReducer,
    auth: authReducer,
});

export const store = createStore(rootReducer, applyMiddleware(middlware));
middlware.run(watchFetchUser);
