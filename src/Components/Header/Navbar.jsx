import React, {Component} from "react";
import { NavLink } from "react-router-dom";
import "../../styles.css";
import Auth from "./Auth";

class Navbar extends Component{
    render(){
        return(
            <nav className="Navbar">
                <div>
                    <NavLink to = "/"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Apple_logo_grey.svg/647px-Apple_logo_grey.svg.png" height="30" alt = "картинка"></img></NavLink>
                </div>
                <div>
                    <NavLink to = "/admin">Admin</NavLink>
                </div>
                <div>
                    <NavLink to = "/user">User</NavLink>
                </div>
                <div>
                    <NavLink to = "/profile">Profile</NavLink>
                </div>
                <div>
                    <Auth/>  
                </div>
            </nav>
        )
    }
}

export default Navbar;