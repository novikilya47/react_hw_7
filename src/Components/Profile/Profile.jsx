import React from 'react';
import {connect} from "react-redux";
import {fetchUser} from '../../Actions/actions';

function Profile(props) {

    const handleEdit = () => {
        props.fetchUser();
    }
    
    return (
        <div className="Info">
            <div>Имя: {props.profile.data.name}</div>
            <div>Email: {props.profile.data.email}</div>
            <button onClick={() => handleEdit()}>Получить пользователя</button>  
        </div>
    )
}

const mapStateToProps = (state) => ({
    profile: state.profile
});

const mapDispatchToProps = (dispatch) => ({
    fetchUser:() => dispatch(fetchUser()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);
