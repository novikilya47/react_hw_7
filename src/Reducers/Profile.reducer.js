import {EDIT_USER, REQUESTED_USER_SUCCEEDED} from "../Constants/constants";

const defaultState = {data: '', name: '', email: ''}

export default function reducer (state = defaultState, action){
    switch (action.type) {
        case EDIT_USER:
            let newState = {
                name: action.data.name,
                email: action.data.email,
                data: action.data
            }
            return newState;
        case REQUESTED_USER_SUCCEEDED:
            let nState = {
                name: action.data.name,
                email: action.data.email,
                data: action.data
            };    
        return nState;
        default:
            return state;
    }
};
