import {FIND_PRODUCT, ADD_PRODUCT, EDIT_PRODUCT, DELETE_PRODUCT, EDIT_USER, RESET_PRODUCT, AUTHENTICATE, REQUESTED_USER_SUCCEEDED, FETCHED_USER} from "../Constants/constants"

export const findProduct = (data) => ({type : FIND_PRODUCT, data : data});
export const resetState = () => ({type : RESET_PRODUCT});
export const addProduct = (data) => ({type : ADD_PRODUCT, data});
export const editProduct = (data) => ({type : EDIT_PRODUCT, data});
export const deleteProduct = (id) => ({type : DELETE_PRODUCT, id});
export const editUser = (data) => ({type : EDIT_USER, data});
export const authenticate = () => ({type : AUTHENTICATE});
export const requestUserSuccess = (data) => ({type : REQUESTED_USER_SUCCEEDED, data: data});
export const fetchUser = () => ({type : FETCHED_USER }); 
