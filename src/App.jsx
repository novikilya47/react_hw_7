import {Provider} from 'react-redux';
import {store} from "./Store/store";
import Profile from "./Components/Profile/Profile";
import Admin from "./Components/Admin/Admin";
import User from "./Components//User/User";
import Home from "./Components/Home/Home";
import Navbar from "./Components/Header/Navbar";
import HOC from './Components/Hoc/Hoc';
import {BrowserRouter} from 'react-router-dom';
import { Route } from "react-router-dom";

function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Navbar />
                <Route exact path='/' component={Home}/>
                <Route path='/admin' component={Admin}/>
                <Route path='/user' component={User}/>
                <HOC path='/profile' component={Profile}/>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
